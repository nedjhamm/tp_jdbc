package EXO1;

public class Commune {

	private String codeInsee;
	private String nomCommune;
	private String codePostal;
	private String libelleAcheminement;
	
	public Commune()
	{
		
	}
	
	public Commune(String codeInsee, String nomCommune, String codePostal, String libelleAcheminement) {
		super();
		this.codeInsee = codeInsee;
		this.nomCommune = nomCommune;
		this.codePostal = codePostal;
		this.libelleAcheminement = libelleAcheminement;
	}

	//getter and setters
	public String getCodeInsee() {
		return codeInsee;
	}
	public void setCodeInsee(String codeInsee) {
		this.codeInsee = codeInsee;
	}
	public String getNomCommune() {
		return nomCommune;
	}
	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getLibelleAcheminement() {
		return libelleAcheminement;
	}
	public void setLibelleAcheminement(String libelleAcheminement) {
		this.libelleAcheminement = libelleAcheminement;
	}
	//--------------------------
	@Override
	public String toString() {
		return "Commune [codeInsee=" + codeInsee + ", nomCommune=" + nomCommune + ", codePostal=" + codePostal
				+ ", libelleAcheminement=" + libelleAcheminement + "]";
	}

	
	
	
}
