package EXO1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mysql.jdbc.PreparedStatement;



public class Main {

	public static void main(String[] args) throws IOException  {
		
		String file="files/laposte_hexasmal.csv";
		Path path= Path.of(file);
		
		List<String> communes=null;
		Map<String,Long> tabCount=null;	
		
		int columnCount = Files.lines(path).findFirst().orElseThrow().split(";").length;
		System.out.println("# Nombre de colonnes: " + columnCount);

		long lineCount = Files.lines(path).count();
		System.out.println("# Nombre de lignes: " + lineCount);

		long nonUniqueCount = Files.lines(path).skip(1).map(line -> line.split(";")[0])
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting())).entrySet().stream()
				.filter(e -> e.getValue() != 1).count();
		System.out.println("# codesInsee non-uniques " +nonUniqueCount+" donc ne peut pas �tre une cl� primaire");
		
		
	//	System.out.println("# commune "+communes.size());
		//------------------
		
		/* hard to spot the non 1 because the list is too long
		for (Map.Entry<String,Long> e: tabCount.entrySet())
		{
			System.out.println("codeInsee unique? " +e.toString());
		}*/
		//-----------------------------
		
	
		
		Function<String, Commune> convertStoC= s->{
				Commune c=new Commune();
				c.setCodeInsee(s.split(";")[0]);
				c.setNomCommune(s.split(";")[1]);
				c.setCodePostal(s.split(";")[2]);
				c.setLibelleAcheminement(s.split(";")[4]);
				return c;
		};
		
		
		
		String url= "jdbc:mysql://localhost:3306/tp_jdbc";
		String user="root";
		String pwd="root";
		
		try(Connection conn=DriverManager.getConnection(url,user,pwd)) {
			conn.setAutoCommit(false);
			CommuneDBService cs=new CommuneDBImpl(conn);
			
			//trials to make sure that my methods work before using them on real data
			
			/*Commune c1= new Commune("11111","Name","22222","Label");
			
			System.out.println("operation rajout reussie? "+cs.writeCommune(c1));
			
			System.out.println("commune extraite de DB"+ cs.getCommuneByName("Name").toString());
			*/
			
			//to import data
			CommuneImporter ci= new CommuneCSVImporter(cs);
			ci.importCommunes(file);
			
			/*String dep="75";
			
			System.out.println("le nombre de communes dans le d�partement "+
			dep+" => "+cs.countCommuneByCP(dep));*/
			
			//does not work on auto commit off mode
			//test
			
			//Commune liste=cs.getCommuneById("33");
			
			conn.commit();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	
		
	}



}
