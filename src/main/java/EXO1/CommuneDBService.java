package EXO1;

import java.util.List;

public interface CommuneDBService {

	boolean writeCommune(Commune commune);
	Commune getCommuneById(String id);
	Commune getCommuneByName(String name);
	public int countCommuneByCP(String codePostal);
}
