package EXO1;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//import com.mysql.jdbc.Statement;

public class CommuneDBImpl implements CommuneDBService {

	private Connection conn;

	
	 public CommuneDBImpl(Connection conn) {
		this.conn=conn;
	
	}
	 
	 
	@Override
	public boolean writeCommune(Commune commune) {
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try(PreparedStatement stat=conn.prepareStatement("insert into communes values (?,?,?,?)",Statement.RETURN_GENERATED_KEYS))
		{
			
			stat.getGeneratedKeys();
			stat.setString(1,commune.getCodeInsee());
			stat.setString(2,commune.getNomCommune());
			stat.setString(3,commune.getCodePostal());
			stat.setString(4,commune.getLibelleAcheminement());
			int res=stat.executeUpdate();
			conn.commit();
			if(res!=0)
				return true;
			else
				return false;
			
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Rajout commune a base de donn�e non r�ussi");
			e.printStackTrace();
		}
	return false;
	
		
	}

	@Override
	public Commune getCommuneByName(String nomCommune) {
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try(PreparedStatement stat=conn.prepareStatement("select * from communes where nomCommune=?"))
		{
		
			Commune c= new Commune();
			stat.setString(1,nomCommune);
			ResultSet rs= stat.executeQuery();
			rs.next();
			c.setCodeInsee(rs.getString(1));
			c.setCodePostal(rs.getString(3));
			c.setNomCommune(rs.getString(2));
			c.setLibelleAcheminement(rs.getString(4));
			conn.commit();
			return c;
						
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Retrait de commune depuis base de donn�e non r�ussi");
			e.printStackTrace();
		}
		return null;
	}
	
	
	public Commune getCommuneById(String id)
	{
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try(PreparedStatement stat=conn.prepareStatement("select * from communes where nomCommune=?",Statement.RETURN_GENERATED_KEYS))
		{
			
			Commune c= new Commune();
			stat.setString(1,id);
			stat.getGeneratedKeys();
			ResultSet rs= stat.executeQuery();
			rs.next();
			c.setCodeInsee(rs.getString(1));
			c.setCodePostal(rs.getString(3));
			c.setNomCommune(rs.getString(2));
			c.setLibelleAcheminement(rs.getString(4));
			System.out.println("Cl�: "+rs.getString(1));
			conn.commit();
			return c;
						
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Retrait de commune depuis base de donn�e non r�ussi");
			e.printStackTrace();
		}
		return null;
	
	}

	public int countCommuneByCP(String codePostal)
	{
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try(PreparedStatement stat=conn.prepareStatement("select count(*) from communes where codePostal like ?"))
		{
		
			//% to say whatever comes after it
			stat.setString(1,codePostal+"%");
			 ResultSet rs=stat.executeQuery();
			 rs.next();
			 conn.commit();
			return rs.getInt(1);
						
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("comptage de communes depuis base de donn�e non r�ussi");
			e.printStackTrace();
		}
		return 0;
	}

	
	
}
