package EXO1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommuneCSVImporter implements CommuneImporter {

	private CommuneDBService comService;
	public CommuneCSVImporter(CommuneDBService comService)
	{
		this.comService = comService;
	}
	
	
	@Override
	public void importCommunes(String path) {
		Path opPath= Path.of(path);
		int i;
		try (Stream<String>lines=Files.lines(opPath))
		{
			List<Commune> mesCommunes=lines.map(convertStoC)
			.collect(Collectors.toList());
			
			
			for (Commune c : mesCommunes)
			{
				comService.writeCommune(c);
				System.out.println("...\n");
			}
			//too slow
			System.out.println("done");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	
		
		
		}
	
	Function<String, Commune> convertStoC= s->{
		Commune c=new Commune();
		c.setCodeInsee(s.split(";")[0]);
		c.setNomCommune(s.split(";")[1]);
		c.setCodePostal(s.split(";")[2]);
		c.setLibelleAcheminement(s.split(";")[4]);
		return c;
};

	}


